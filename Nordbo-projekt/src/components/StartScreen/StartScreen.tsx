import Heading from "../Text/Heading"
import StartBackground from "../../assets/backgrounds/StartBackground.png"
import wavinghand from "../../assets/emojis/wavinghand.svg"
import StartButton from "./StartButton"

interface StartScreenProps {
  onStart: () => void
  startscreenRef: React.RefObject<HTMLDivElement>
}

export default function StartScreen({
  onStart,
  startscreenRef,
}: StartScreenProps) {
  const headerText =
    "Let’s try and find the \n solution that matches your \n clients needs the best"
  return (
    <div className="StartScreenWrapper" ref={startscreenRef}>
      <div className="WelcomeHeader">
        <Heading
          type="h5"
          weight="600"
          size="24"
          lineHeight="29"
          text="Welcome"
          color="#FFFFFF"
        />
        <img className="WelcomeIcon" src={wavinghand} alt="waving hand" />
      </div>
      <Heading
        type="h3"
        weight="700"
        size="64"
        lineHeight="77"
        text={headerText}
        color="#FFFFFF"
        textAlign="center"
      />
      <StartButton onClick={onStart} />
      <style jsx>{`
        .StartScreenWrapper {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          border-radius: 8px;
          margin: 43px 80px 100px 80px;
          padding: 150px 240px;
          background-image: url(${StartBackground});
          background-size: cover;
        }
        .WelcomeHeader {
          display: flex;
          gap: 12px;
        }
        .WelcomeIcon {
        }
      `}</style>
    </div>
  )
}
