import Parapgraph from "../Text/Parapgraph"
import StartscreenArrowDown from "../../assets/icons/StartscreenArrowDown.svg"

interface StartButtonProps {
  onClick: () => void
}

export default function StartButton({ onClick }: StartButtonProps) {
  return (
    <button className="StartButton" onClick={() => onClick()}>
      <div className="StartButtonTextWrapper">
        <Parapgraph
          text="I'm ready, let's start"
          size="18"
          weight="600"
          lineHeight="22"
          color="#fff"
        ></Parapgraph>
        <img
          className="StartButtonIcon"
          src={StartscreenArrowDown}
          alt="arrow down"
        />
      </div>
      <style jsx>{`
        .StartButton {
          display: flex;
          justify-content: center;
          align-items: center;
          margin-top: 82px;
          padding: 17px 48px;
          background: #62c3e9;
          border-radius: 12px;
          border: none;
        }
        .StartButtonTextWrapper {
          display: flex;
          gap: 15px;
        }
        .StartButtonIcon {
        }
      `}</style>
    </button>
  )
}
