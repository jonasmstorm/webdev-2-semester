import Parapgraph from "../Text/Parapgraph"
import "mdb-react-ui-kit/dist/css/mdb.min.css"
import "@fortawesome/fontawesome-free/css/all.min.css"
import React from "react"

import TableRow from "./TableRow"
const Rows = [
  {
    title: "Features",
    rows: [
      {
        blue: true,
        text: "Point to point",
        icons: [0, 0, 0, 0, 1, 0],
      },
      {
        blue: false,
        text: "Program (recipe) building",
        icons: [0, 0, 1, 1, 1, 0],
      },
      {
        blue: true,
        text: "Force control",
        icons: [0, 0, 0, 1, 1, 1],
      },
      {
        blue: false,
        text: "Path simulation",
        icons: [0, 0, 0, 1, 1, 1],
      },
    ],
  },
  {
    title: "Robot type",
    rows: [
      {
        blue: true,
        text: "Industrial",
        icons: [0, 0, 1, 0, 1, 1],
      },
      {
        blue: false,
        text: "Collaborative",
        icons: [1, 1, 1, 1, 1, 1],
      },
    ],
  },
  {
    title: "Production type",
    rows: [
      {
        blue: true,
        text: "Low mix, High volume",
        icons: [1, 1, 1, 1, 0, 1],
      },
      {
        blue: false,
        text: "High-mix, low volume",
        icons: [0, 0, 0, 0, 1, 0],
      },
    ],
  },
  {
    title: "Ease of use",
    rows: [
      {
        blue: true,
        text: "Custom joystick",
        icons: [1, 1, 1, 1, 0, 0],
      },
      {
        blue: false,
        text: "Tactile/visual feedback",
        icons: [0, 1, 0, 1, 1, 1],
      },
    ],
  },
  {
    title: "Precision",
    rows: [
      {
        blue: true,
        text: "High",
        icons: [0, 1, 0, 1, 1, 1],
      },
      {
        blue: false,
        text: "Medium",
        icons: [1, 0, 1, 0, 0, 0],
      },
    ],
  },
  {
    title: "Application",
    rows: [
      {
        blue: true,
        text: "Point to point",
        icons: [0, 0, 0, 0, 1, 0],
      },
      {
        blue: false,
        text: "Program (recipe) building",
        icons: [0, 0, 1, 1, 1, 0],
      },
      {
        blue: true,
        text: "Force control",
        icons: [0, 0, 0, 1, 1, 1],
      },
      {
        blue: false,
        text: "Path simulation",
        icons: [0, 0, 0, 1, 1, 1],
      },
    ],
  },
]
import {
  MDBContainer,
  MDBTable,
  MDBTableBody,
  MDBTableHead,
} from "mdb-react-ui-kit"
export default function SolutionComparison() {
  return (
    <div className="result-modal__content-solution-comparison">
      <Parapgraph
        text={"Solution Comparison"}
        size="64"
        lineHeight="58"
        weight="700"
        color="#fff"
      />
      <div className="result-modal__content-comparison-table">
        <div className="result-modal__content-solution">
          <div className="result-modal__content-match">
            <Parapgraph
              text={"Best match"}
              size="13"
              lineHeight="17"
              weight="600"
              color="#1D6AFA"
            />
          </div>
          <div className="result-modal__content-solution-border"></div>
        </div>
        <MDBContainer className="py-5">
          <MDBTable
            responsive
            striped
            className=" text-successtable-border border-light"
          >
            <MDBTableHead className="border-light">
              <tr>
                <th scope="col"></th>
                <th scope="col">
                  <Parapgraph
                    text={"MIMIC"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#082965"
                  />
                  <br />
                  <Parapgraph
                    text={"IR"}
                    size="18"
                    lineHeight="24"
                    weight="600"
                    color="#999999"
                  />
                </th>
                <th scope="col">
                  <Parapgraph
                    text={"URCap"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#082965"
                  />
                  <br />
                  <Parapgraph
                    text={"FT"}
                    size="18"
                    lineHeight="24"
                    weight="600"
                    color="#999999"
                  />
                </th>
                <th scope="col">
                  <Parapgraph
                    text={"Mimic Core"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#082965"
                  />
                  <br />
                  <Parapgraph
                    text={"IR"}
                    size="18"
                    lineHeight="24"
                    weight="600"
                    color="#999999"
                  />
                </th>
                <th scope="col">
                  <Parapgraph
                    text={"Mimic Sander"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#082965"
                  />
                  <br />
                  <Parapgraph
                    text={"FT"}
                    size="18"
                    lineHeight="24"
                    weight="600"
                    color="#999999"
                  />
                </th>
                <th scope="col">
                  <Parapgraph
                    text={"CraftMate"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#1D6AFA"
                  />
                  <br />
                  <span className="result-modal__content-transparent">f</span>
                </th>
                <th scope="col">
                  <Parapgraph
                    text={"Buffing"}
                    size="18"
                    lineHeight="22"
                    weight="600"
                    color="#082965"
                  />
                  <br />
                  <span className="result-modal__content-transparent">f</span>
                </th>
              </tr>
            </MDBTableHead>
            <MDBTableBody>
              {Rows.map((row, index) => (
                <React.Fragment key={index}>
                  <tr className="result-modal__content-table-section">
                    <th
                      className="result-modal__content-table-header"
                      scope="row"
                    >
                      <Parapgraph
                        text={row.title}
                        size="24"
                        lineHeight="24"
                        weight="600"
                        color="#082965"
                      />{" "}
                    </th>
                  </tr>
                  <tr className="result-modal__content-spacing"></tr>
                  {row.rows.map((row, key) => (
                    <TableRow
                      key={key}
                      blue={row.blue}
                      text={row.text}
                      icons={row.icons}
                    />
                  ))}
                </React.Fragment>
              ))}
            </MDBTableBody>
          </MDBTable>
        </MDBContainer>
      </div>
      <style jsx>{`
        .result-modal__content-solution-comparison {
          min-height: 1950px;
          margin-top: -330px;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .result-modal__content-comparison-table {
          background: #fff;
          border-radius: 8px;
          margin-top: 70px;
          box-shadow: 0px 4px 40px 0px #0000001f;
          position: relative;
          padding-top: 20px;
        }
        .result-modal__content-solution {
          position: absolute;
          top: 30px;
          left: 74.5%;
          height: 93%;
        }
        .result-modal__content-match {
          display: flex;
          align-items: center;
          justify-content: center;
          margin-bottom: 20px;
        }
        .result-modal__content-solution-border {
          border: 3px solid #1d6afa;
          width: 131px;
          height: 100%;
          border-radius: 12px;
        }
        .result-modal__content-table-header {
          background: #fff !important;
          --mdb-table-accent-bg: #fff !important;
          padding: 1rem 0px !important;
        }
        .result-modal__content-table-section {
          border-bottom: 0.85px solid #d3d3d3;
        }
        .result-modal__content-transparent {
          opacity: 0;
        }
        .result-modal__content-spacing {
          height: 20px;
        }
        .border-light {
          border-color: #fff !important;
        }
      `}</style>
    </div>
  )
}
