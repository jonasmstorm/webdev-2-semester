import Parapgraph from "../Text/Parapgraph"
import ModalBackground from "../../assets/backgrounds/ModalBackground.png"
import checkmark from "../../assets/icons/resultCheckmark.svg"

const boxes = [
  {
    text: "Good for for high mix/low volume & high tolerances",
    box: 1,
  },
  {
    text: "Flexibility in fixtures/mounting",
    box: 2,
  },
  {
    text: "Possibility to auto adjust to a CAD/CAM",
    box: 3,
  },
  {
    text: "No know-how needed & ensures quality of output",
    box: 4,
  },
  {
    text: "Possiblity to work on large parts (stitching features)",
    box: 5,
  },
  {
    text: "Fast programming of waypoints",
    box: 6,
  },
  {
    text: "Orbital sanders, die grinders & angle grinders",
    box: 7,
  },
]

export default function ResultContent() {
  return (
    <div className="result-modal__content-wrapper">
      <div className="result-modal__content-overview">
        <div className="result-modal__content-overview-item">
          <Parapgraph
            text={"83%"}
            size="96"
            lineHeight="58"
            weight="700"
            color="#fff"
          />
          <Parapgraph
            text={"Manual manpower reduction"}
            size="14"
            lineHeight="17"
            weight="500"
            color="#fff"
          />
        </div>
        <div className="result-modal__content-overview-item">
          <Parapgraph
            text={"30x"}
            size="96"
            lineHeight="58"
            weight="700"
            color="#fff"
          />
          <Parapgraph
            text={"Faster than industrial robot programming"}
            size="14"
            lineHeight="17"
            weight="500"
            color="#fff"
          />
        </div>
        <div className="result-modal__content-overview-item">
          <Parapgraph
            text={"20+"}
            size="96"
            lineHeight="58"
            weight="700"
            color="#fff"
          />
          <Parapgraph
            text={"Supported robot types"}
            size="14"
            lineHeight="17"
            weight="500"
            color="#fff"
          />
        </div>
      </div>
      <div className="result-modal__content-border"></div>

      <div className="result-modal__content-boxes-wrapper">
        <Parapgraph
          text={"Why CraftMate"}
          size="64"
          lineHeight="58"
          weight="700"
          color="#fff"
        />
        <div className="result-modal__content-boxes">
          {boxes.map(({ text, box }) => (
            <div className="result-modal__content-box" key={box}>
              <div className="result-modal__content-box-checkmark">
                <img src={checkmark} />
              </div>
              <Parapgraph
                text={text}
                size="24"
                lineHeight="24"
                weight="600"
                color="#fff"
              />
            </div>
          ))}
        </div>
      </div>
      <div className="result-modal__content-border"> </div>

      <style jsx>{`
        .result-modal__content-wrapper {
          background-image: url(${ModalBackground});
          background-size: cover;
          background-repeat: no-repeat;
          min-height: 1850px;
          height: fit-content;
        }
        .result-modal__content-overview {
          display: flex;
          justify-content: space-between;
          padding: 145px 145px 60px 145px;
        }
        .result-modal__content-overview-item {
          display: flex;
          flex-direction: column;
          gap: 50px;
        }
        .result-modal__content-border {
          width: 95%;
          height: 3px;
          background: #fff;
          border-radius: 4px;
          margin: 0 auto;
        }
        .result-modal__content-boxes-wrapper {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          margin-top: 170px;
        }
        .result-modal__content-boxes {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
          width: 80%;
          margin-top: 80px;
        }
        .result-modal__content-box {
          background: #082965;
          border: 1px solid #fff;
          border-radius: 4px;
          height: 200px;
          width: 32%;
          padding: 40px 24px;
          display: flex;
          text-align: center;
          justify-content: center;
          align-items: center;
          margin-bottom: 60px;
          position: relative;
        }
        .result-modal__content-box-checkmark {
          position: absolute;
          display: flex;
          align-items: center;
          justify-content: center;
          width: 72px;
          height: 72px;
          border-radius: 50%;
          background: #082965;
          border: 1px solid #fff;
          top: -35px;
          left: 20px;
        }
      `}</style>
    </div>
  )
}
