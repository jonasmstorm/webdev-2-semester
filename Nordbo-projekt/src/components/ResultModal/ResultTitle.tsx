import Parapgraph from "../Text/Parapgraph"
import arrowDownWhite from "../../assets/icons/arrowDownWhite.svg"
const text = "Power tool for vision-based surface finishing\n processes.Scan and construct a highly\n precise 3D model of a surface to be processed\n with a press on a button.";

export default function ResultTitle() {

  return (
    <div className="result-modal__content-result-title">
        <video className='result-modal__content-videoTag' autoPlay loop muted>
            <source src="https://s3-figma-videos-production-sig.figma.com/video/1162312037920449131/TEAM/693c/5d75/-83d4-4c1d-baef-739e4f807050?Expires=1685318400&Signature=mrOqXPvKd3LjyNY2ICdyLEsovAetqQMamJePW74RrLBozWCctnfk24ejQfBOcEqZZvL391n8i-yMpNsDmkUu8r8og4RQVr29qf2-1Yw~q2JdyUrQ7RpwUHbscwDo-soXMdI86eaT0LwZuBvB9eMVrtXFabOisJS8LF4bYQY5ecZnbjPy0D9GqIdQKfJPIYZLGv7hnMQUkhxCAyunHBMDTzn2PnaiR2zXKihuAqg3tBN5iu4sJX5nyn5Bona3AO1bt8f7riXuIbmWXQkgpJemChMEo4sSwj6MG~it-oUgTKCdgj8k68CeougF8TjWo4NujQ4Df9k9rOpokLH9mk97MA__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" type='video/mp4' />
        </video>
      <div className="result-modal__content-title-div">
      <div className="result-modal__content-best-match">
      <Parapgraph
                text={"BEST MATCH"}
                size="20"
                lineHeight="24"
                weight="600"
                color="#082965"
              />
      </div>
      <Parapgraph
                text={"CraftMate"}
                size="72"
                lineHeight="58"
                weight="600"
                color="#fff"
              />
        <div className="result-modal__content-result-text">
        <Parapgraph
                text={text}
                size="48"
                lineHeight="58"
                weight="600"
                color="#fff"
              />
      </div>
      <div className="result-modal__content-result-scroll">
      <img src={arrowDownWhite}/>
      <Parapgraph
                text={"Scroll for more"}
                size="14"
                lineHeight="17"
                weight="400"
                color="#fff"
              />
      </div>
      </div>
      <style jsx>{`
        .result-modal__content-result-title {
            background: #082965;
            height: 85%;
            position: relative;
            overflow: hidden;
      }
      .result-modal__content-result-title:before {
        content:"";
        position: absolute;
        top:0;
        right:0;
        left:0;
        bottom:0;
        z-index:1;
        background: #082965;
        opacity: 0.8;
      }
      .result-modal__content-title-div {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        text-align: center;
        height: 100%;
        width: 100%;
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        color: #fff;
        z-index:1;
      }

      .result-modal__content-best-match{
        background: #fff;
        padding: 8px 16px;
        border-radius: 8px;
        margin-top: 120px;
        margin-bottom: 20px;
      }

      .result-modal__content-result-text {
        margin-top: 40px;
        width: 70%;
        white-space: pre-line;
      }

      .result-modal__content-result-scroll {
        margin-top: auto;
        margin-bottom: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        gap: 5px;
      }

      .result-modal__content-videoTag {
       position: absolute;
       width: 100vw;
      }


        }
      `}</style>
    </div>
  )
}
