import Parapgraph from "../Text/Parapgraph"
import TableCheckmark from "../../assets/icons/TableCheckmark.svg"
import TableCross from "../../assets/icons/TableCross.svg"

interface TableRowProps {
   blue: boolean
   text: string
   icons: Array<number>
  }
  
export default function TableRow({blue,text,icons}:TableRowProps) {
  return (
    <tr className={blue ? "blue" : ""}>
   <th scope="row">
    <Parapgraph
        text={text}
        size="14"
        lineHeight="17"
        weight="600"
        color="#082965"
            />
    </th>
    {icons.map((icon,index) => (
        <td key={index}  align="center">
            <img src={icon == 0 ? TableCross : TableCheckmark}/>
        </td>
    ))}
      <style jsx>{`
        .table-striped>tbody>tr>* {
        --mdb-table-accent-bg: #fff;
        color: var(--mdb-table-striped-color);
        }
        .table-striped>tbody>tr.blue>* {
            --mdb-table-accent-bg: #F2F6FF!important;
            color: var(--mdb-table-striped-color);
        }
        .table-striped>tbody>tr:nth-of-type(odd)>* {
            --mdb-table-accent-bg: #fff;
        }
      `}</style>
    </tr>
  )
}