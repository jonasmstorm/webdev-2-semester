

import { useState, Dispatch, SetStateAction, RefObject } from "react"
import ResultHeader from "../Header/ResultHeader"
import ResultTitle from "./ResultTitle"
import ResultContent from "./ResultContent"
import Parapgraph from "../Text/Parapgraph"
import SolutionComparison from "./SolutionComparison"

interface ResultProps {
  show: boolean
  RestartTool: () => void
} 




export default function Result({show, RestartTool}: ResultProps) {
  const [started, setStarted] = useState(true)

  return (
    <div className="result-modal">
      <ResultHeader RestartTool={RestartTool} started={started} />
      <div className="result-modal__content">
      <ResultTitle />
      <ResultContent />
      <SolutionComparison />

      </div>
      <style jsx>{`
        .result-modal {
          position: fixed;
          z-index: 1;
          opacity: ${show ? "1" : "0"};
          background: #fff;
          left: 0;
          border-radius: 8px;
          width: 80%;
          height: 90%;
          top:  ${show ? "3%" : "100%"};
          transition: all 500ms ease-in-out, opacity .01s linear;
          right: 0;
          margin: 0 auto;
          bottom: 0;
          overflow: hidden;
      }
      .result-modal__content {
        overflow-y: scroll;
        position: relative;
        overflow-x: hidden;
        height: 100%;
        width: 100%;
      }

 
      /* scrollbar styling */
      /* width */
        ::-webkit-scrollbar {
          width: 12px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
          background: #F3F3F3;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
          background: #C6C6C6;
          border-radius: 12px
        }
        }
      `}</style>
    </div>
  )
}
