interface ParapgraphProps {
  text?: string
  color?: string
  xxl?: boolean
  xl?: boolean
  l?: boolean
  m?: boolean
  s?: boolean
  xs?: boolean
}

export default function Parapgraph({
  text,
  xxl,
  xl,
  l,
  m,
  s,
  xs,
}: ParapgraphProps) {
  //Bold = 600
  //Semi-bold = 500
  //Regular = 400
  const fontCategory = xxl
    ? "xxl"
    : xl
    ? "xl"
    : l
    ? "l"
    : m
    ? "m"
    : s
    ? "s"
    : xs
    ? "xs"
    : "default"
  const fontWeightMap = {
    xxl: "500",
    xl: "600",
    l: "500",
    m: "600",
    s: "400",
    xs: "400",
    default: "400",
  }
  const lineHeightMap = {
    xxl: "72px",
    xl: "54px",
    l: "36px",
    m: "27px",
    s: "21px",
    xs: "18px",
    default: "24px",
  }
  const fontSizeMap = {
    xxl: "48px",
    xl: "36px",
    l: "24px",
    m: "18px",
    s: "14px",
    xs: "12px",
    default: "16px",
  }
  return (
    <div className="P">
      {text}
      <style jsx>
        {`
          .P {
            color: #1d6afa;
            font-weight: ${fontWeightMap[fontCategory]};
            line-height: ${lineHeightMap[fontCategory]};
            font-size: ${fontSizeMap[fontCategory]};
          }
        `}
      </style>
    </div>
  )
}
