interface ParapgraphProps {
  text?: string
  color?: string
  size?: string
  weight?: "400" | "500" | "600" | "700"
  lineHeight?: "17" | "19" | "22" | "24" | "36" | "58"
}

export default function Parapgraph({
  text,
  color,
  size,
  weight,
  lineHeight,
}: ParapgraphProps) {
  const fontMap = {
    "400": "inherit",
    "500": "inherit",
    "600": "interSemiBold",
    "700": "interBold",
  }

  const mystyle = {
    color: color ? color : "#1d6afa",
    fontSize: (size ? size : "16") + "px",
    fontFamily: fontMap[weight ? weight : "400"],
    lineHeight: (lineHeight ? lineHeight : "24") + "px",
    transition: "all 1s ease-in",
  }
  return <p style={mystyle}>{text}</p>
}
