interface HeadingProps {
  type?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"
  text?: string
  color?: string
  size?: string
  weight?: "600" | "700"
  lineHeight?: "22" | "29" | "39" | "77" | "87" | "116"
  textAlign?: "left" | "center" | "right"
  whiteSpace?: "pre-wrap" | "pre-line"
}

export default function Heading({
  text,
  color,
  size,
  weight,
  lineHeight,
  type,
  textAlign,
  whiteSpace,
}: HeadingProps) {
  const fontMap = {
    "600": "interSemiBold",
    "700": "interBold",
  }

  const myStyle = {
    whiteSpace: whiteSpace ? whiteSpace : "pre-wrap",
    color: color ? color : "#082965",
    fontSize: (size ? size : "39") + "px",
    fontFamily: fontMap[weight ? weight : "600"],
    lineHeight: (lineHeight ? lineHeight : "32") + "px",
    transition: "all 1s ease-in",
    textAlign: textAlign ? textAlign : "left",
  }

  const heading1 = (text: string) => <h1 style={myStyle}>{text}</h1>
  const heading2 = (text: string) => <h2 style={myStyle}>{text}</h2>
  const heading3 = (text: string) => <h3 style={myStyle}>{text}</h3>
  const heading4 = (text: string) => <h4 style={myStyle}>{text}</h4>
  const heading5 = (text: string) => <h5 style={myStyle}>{text}</h5>
  const heading6 = (text: string) => <h6 style={myStyle}>{text}</h6>

  const typeChart = {
    h1: heading1,
    h2: heading2,
    h3: heading3,
    h4: heading4,
    h5: heading5,
    h6: heading6,
  }
  const ChosenHeading = type ? typeChart[type] : heading1
  return ChosenHeading(text ?? "")
}
