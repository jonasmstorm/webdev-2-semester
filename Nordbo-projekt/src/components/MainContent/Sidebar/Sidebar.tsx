import Parapgraph from "../../Text/Parapgraph"
import SidebarBackground from "../../../assets/backgrounds/SidebarBackground.png"
import { useState } from "react"
import checkmark from "../../../assets/icons/checkmark.svg"

interface QuestionsProps {
  stage: number
}

const steps = [
  {
    label: 'Type needed',
    step: 1,
  },
  {
    label: 'Job difficulties',
    step: 2,
  },
  {
    label: 'Production challenges',
    step: 3,
  },
  {
    label: 'Human errors',
    step: 4,
  },
  {
    label: "Automation",
    step: 5,
  },
]

export default function Sidebar({ stage }: QuestionsProps) {

  

  const totalSteps = steps.length

  const width = `${(100 / (totalSteps - 1)) * (stage - 1)}%`

  return ( 
    <div className="sidebar" >
          <div className="sidebar__container" >
          <div className="sidebar__title" >
        <Parapgraph  text={"Overview"}
                size="32"
                lineHeight="36"
                weight="700"
                color="white" />
                            </div>

      <div className="sidebar__step-container">
      {steps.map(({ step, label }) => (
          <div className="sidebar__step-wrapper" key={step}>
            <div className={stage >= step ? 'sidebar__step-style sidebar__step-style--completed' : 'sidebar__step-style sidebar__step-style--incomplete'}>
              {stage > step ? (
                <div className="sidebar__checkmark"> <img src={checkmark}/></div>
              ) : (
                <Parapgraph  text={step.toString()}
                size="20"
                lineHeight="24"
                weight="700"
                color={stage  >= step ? "#1D6AFA" : "#FFF"} />
              )}
            </div>
            <div className="sidebar__step-steps-label-container">
              <Parapgraph text={label}
                size="16"
                lineHeight="19"
                weight="400"
                color="#fff" key={step} />
            </div>
          </div>
        ))}
      </div>
      </div>
      <style jsx>
        {`
          .sidebar {
            display: flex;
            min-width: 23%; /* Sidebar = 338px, Container = 1129px */
            max-width: 23%;
            min-height: 822px; /* bare til testing */
            background-image: url(${SidebarBackground});
            background-size: cover;
            background-repeat: no-repeat;
          }
          .sidebar__title {
            padding: 20px 0px;
          }
          .sidebar__container {
            width: 80%;
            height: fit-content;
            padding: 20px;
            background: #103375;
            margin: auto;       
          }
          .sidebar__step-container {
            display: flex;
          justify-content: space-between;
          flex-direction: column;
          position: relative;
          min-height: 500px;
          }
          .sidebar__step-container::before {
              content: '';
              position: absolute;
              background: #081E46;
              height: 100%;
              width: 4px;
              left: 6.5%;
              transform: translateX(-50%);
          }
          .sidebar__step-container::after {
              content: '';
              position: absolute;
              background: #fff;
              height:  ${width};
              width: 4px;
              transition: 0.4s ease;
              transform: translateX(-50%);
              left: 6.5%;
          }
        .sidebar__checkmark {
          display: flex;
          align-items: center;
          justify-content: center;
          width: 40px;
          height: 40px;
          border-radius: 50%;
          transition: 0.4s ease;
          background:#1D6AFA;
        }
          .sidebar__step-wrapper {
            position: relative;
            z-index: 1;
            display: flex;
            align-items: center;
          }
          .sidebar__step-style {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            background-color: #ffffff;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          .sidebar__step-style--completed {
            border: 3px solid #1D6AFA;
          }
          .sidebar__step-style--incomplete {
            background: #4A6493;
          }
          .sidebar__step-steps-label-container {
           margin-left: 10px;
          }

        `}
      </style>
    </div>
  )
}
