import Parapgraph from "../../Text/Parapgraph"
import Type from "../../QuestionPages/Type"
import JobDifficulties from "../../QuestionPages/JobDifficulties"
import HumanError from "../../QuestionPages/HumanErrors"
import Automation from "../../QuestionPages/Automation"
import ProductionChallenge from "../../QuestionPages/ProductionChallenges"
import EmptyPage from "../../QuestionPages/EmptyPage"
import { useState, useRef, useEffect } from "react"

interface QuestionsProps {
  stage?: number
  back: () => void
  next: () => void
}

export default function Questions({ stage, back, next }: QuestionsProps) {
  const [selectedTypes, setSelectedTypes] = useState<string[]>([])
  const [selectedJob, setSelectedJob] = useState<string>("")
  const [selectedProd, setSelectedProd] = useState<string>("")
  const [selectedHumanError, setSelectedHumanError] = useState<string>("")
  const [selectedAutomation, setSelectedAutomation] = useState<string>("")

  const typeRef = useRef<HTMLDivElement>(null)
  const jobRef = useRef<HTMLDivElement>(null)
  const prodRef = useRef<HTMLDivElement>(null)
  const humanRef = useRef<HTMLDivElement>(null)
  const autoRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (stage === 1) {
      typeRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    if (stage === 2) {
      jobRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    if (stage === 3) {
      prodRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    if (stage === 4) {
      humanRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    if (stage === 5) {
      autoRef.current?.scrollIntoView({ behavior: "smooth" })
    }
  }, [stage])

  return (
    <div className="QuestionsWrapper">
      <div className="QuestionList">
        <Type
          next={() => next()}
          setSelectedTypes={setSelectedTypes}
          selectedTypes={selectedTypes}
          typeRef={typeRef}
        />

        <JobDifficulties
          next={() => next()}
          back={() => back()}
          setSelectedJob={setSelectedJob}
          selectedJob={selectedJob}
          jobRef={jobRef}
        />

        <ProductionChallenge
          next={() => next()}
          back={() => back()}
          setSelectedProd={setSelectedProd}
          selectedProd={selectedProd}
          prodRef={prodRef}
        />

        <HumanError
          next={() => next()}
          back={() => back()}
          setSelectedHumanError={setSelectedHumanError}
          selectedHumanError={selectedHumanError}
          humanRef={humanRef}
        />

        <Automation
          next={() => next()}
          back={() => back()}
          setSelectedAutomation={setSelectedAutomation}
          selectedAutomation={selectedAutomation}
          autoRef={autoRef}
        />
        <EmptyPage />
      </div>

      <style jsx>
        {`
          .QuestionsWrapper {
            display: flex;
            flex-direction: column;
            height: auto;
            min-width: 77%; /* Sidebar = 338px, Container = 1129px */
            max-width: 77%;
            background: white;
          }
          .QuestionList {
            display: flex;
            flex-direction: column;
            overflow-y: hidden;
            height: 100%;
            container-type: size;
          }
        `}
      </style>
    </div>
  )
}
