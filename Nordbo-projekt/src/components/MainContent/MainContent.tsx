import Sidebar from "./Sidebar/Sidebar"
import Questions from "./Questions/Questions"

interface MainContentProps {
  stage: number
  back: () => void
  next: () => void
  mainContentRef: React.RefObject<HTMLDivElement>
}

export default function MainContent({
  stage,
  back,
  next,
  mainContentRef,
}: MainContentProps) {
  return (
    <div className="MainContent" ref={mainContentRef}>
      <Sidebar stage={stage}></Sidebar>
      <Questions stage={stage} back={back} next={next}></Questions>
      <style jsx>{`
        .MainContent {
          display: flex;
          width: 100%;
          height: 100%;
        }
      `}</style>
    </div>
  )
}
