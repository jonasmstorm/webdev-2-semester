import { useState, useRef, useEffect } from "react"
import Header from "../Header/Header"
import HeaderProgressBar from "../Header/HeaderProgressBar"
import MainContent from "../MainContent/MainContent"
import Result from "../ResultModal/Result"
import StartScreen from "../StartScreen/StartScreen"

type stageType = 0 | 1 | 2 | 3 | 4 | 5

function App() {
  const [started, setStarted] = useState(true)
  const [stage, setStage] = useState(0)
  const [finished, setFinished] = useState(false)
  const startscreenRef = useRef<HTMLDivElement>(null)
  const mainContentRef = useRef<HTMLDivElement>(null)

  function Back() {
    if (stage === 0) return
    setStage(stage - 1)
  }

  function Next() {
    if (stage === 5) {
      setFinished(true)
      setStarted(false)
    } else {
      setStage(stage + 1)
    }
  }

  function Exit() {
    setStarted(false)
    setStage(0)
    startscreenRef.current?.scrollIntoView({ behavior: "smooth", block: "end" })
  }

  function RestartTool() {
    setStarted(true)
    setFinished(false)
    setStage(1)
  }
  function onStart() {
    setStage(1)
    mainContentRef.current?.scrollIntoView({ behavior: "smooth" })
  }

  return (
    <div className="App">
      <Header Exit={Exit} started={started} />
      <HeaderProgressBar stage={stage as stageType} />
      <div className="StartScreenTransition">
        <StartScreen
          startscreenRef={startscreenRef}
          onStart={onStart}
        ></StartScreen>
        <MainContent
          mainContentRef={mainContentRef}
          stage={stage}
          back={Back}
          next={Next}
        />
      </div>
      <div className={finished ? "ResultWrapper visible" : "ResultWrapper"}>
        {" "}
      </div>
      <Result RestartTool={RestartTool} show={finished} />

      <style jsx>
        {`
          .App {
          }
          .StartScreenTransition {
            display: flex;
            flex-direction: column;
            overflow-y: hidden;
            height: 100%;
            max-height: 87vh;
          }

          .ResultWrapper.visible {
            visibility: visible;
            opacity: 0.1;
          }
          .ResultWrapper {
            visibility: hidden;
            opacity: 0;
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            z-index: 1;
            background-color: #000;
            transition: opacity 500ms, visibility 500ms;
          }
        `}
      </style>
    </div>
  )
}

export default App
