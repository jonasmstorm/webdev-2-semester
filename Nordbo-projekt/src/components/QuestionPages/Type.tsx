import { Dispatch, SetStateAction, RefObject } from "react"
import Parapgraph from "../Text/Parapgraph"
import TypeCard from "./TypeCards/TypeCard"
import CardNavigationButton from "./SharedCards/CardNavigationButton"
interface TypeProps {
  next: () => void
  setSelectedTypes: Dispatch<SetStateAction<string[]>>
  selectedTypes: string[]
  typeRef: RefObject<HTMLDivElement>
}

interface OptionProps {
  text: string
  image:
    | "sanding"
    | "deburring"
    | "sprayPaint"
    | "grinding"
    | "polishing"
    | "qualityControl"
}

const options: OptionProps[] = [
  {
    text: "Sanding",
    image: "sanding",
  },
  {
    text: "Deburring",
    image: "deburring",
  },
  {
    text: "Spray Painting",
    image: "sprayPaint",
  },
  {
    text: "Grinding",
    image: "grinding",
  },
  {
    text: "Polishing",
    image: "polishing",
  },
  {
    text: "Quality Control",
    image: "qualityControl",
  },
]

export default function Type({
  next,
  selectedTypes,
  setSelectedTypes,
  typeRef,
}: TypeProps) {
  return (
    <div className="Type" ref={typeRef}>
      <div className="CardIndexContainer">
        <div className="CardIndex">1</div>
      </div>
      <div className="TypeContent">
        <div className="TypeHeaderContainer">
          <div className="TypeHeader">
            <Parapgraph
              text="What product/application type is needed?"
              size="28"
              lineHeight="36"
              weight="700"
              color="#082965"
            />
            <Parapgraph
              text="Please select the answer that fits the best"
              size="16"
              lineHeight="19"
              weight="400"
              color="#818181"
            />
          </div>
        </div>
        <div className="TypeQuestionContainer">
          {options.map(({ text, image }, i) => (
            <TypeCard
              key={i}
              text={text}
              image={image}
              selected={selectedTypes.some((type) => type === text)}
              onClick={() => {
                if (selectedTypes.some((type) => type === text)) {
                  setSelectedTypes((prev) =>
                    prev.filter((type) => type !== text)
                  )
                } else {
                  setSelectedTypes((prev) => [...prev, text])
                }
              }}
            />
          ))}
        </div>
        <div className="TypeContinueButtonWrapper">
          <CardNavigationButton
            text="Continue"
            filled
            icon
            onClick={() => next()}
            disabled={selectedTypes.length === 0}
          />
        </div>
      </div>
      <style jsx>
        {`
          .Type {
            display: flex;
            padding: 98px 89px;
            height: 100%;
          }
          .TypeContent {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
          }
          .TypeHeaderContainer {
            display: flex;
            width: 100%;
            margin-bottom: 32px;
          }
          .CardIndexContainer {
            margin-right: 16px;
            padding: 6px 0px;
          }
          .CardIndex {
            width: 32px;
            height: 32px;
            border-radius: 50%;
            background: #1d6afa;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 700;
          }
          .TypeHeader {
            display: flex;
            flex-direction: column;
            gap: 8px;
          }
          .TypeQuestionContainer {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            gap: 16px;
          }
          .TypeContinueButtonWrapper {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: end;
            margin-top: 40px;
          }
        `}
      </style>
    </div>
  )
}
