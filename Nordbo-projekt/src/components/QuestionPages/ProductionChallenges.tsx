import SharedLayout from "./SharedCards/SharedLayout"
import { useState, Dispatch, SetStateAction } from "react"

interface ProductionChallengeProps {
  next: () => void
  back: () => void
  setSelectedProd: Dispatch<SetStateAction<string>>
  selectedProd: string
  prodRef: React.RefObject<HTMLDivElement>
}

interface OptionProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
}

const options: OptionProps[] = [
  {
    text: "Yes, very challenging",
    image: "thumb-up",
  },
  {
    text: "Somewhat challenging",
    image: "thumb-pinch",
  },
  {
    text: "No, not challenging",
    image: "thumb-down",
  },
]

export default function ProductionChallenge({
  next,
  back,
  setSelectedProd,
  selectedProd,
  prodRef,
}: ProductionChallengeProps) {
  const header =
    "Is it challenging meeting production goals while complying with worker safety regulations?"
  const question = "Please select the answer that fits the best"
  const cardIndex = 3
  return (
    <SharedLayout
      back={back}
      next={next}
      setSelectedItem={setSelectedProd}
      selectedItem={selectedProd}
      options={options}
      header={header}
      question={question}
      cardIndex={cardIndex}
      sharedRef={prodRef}
    />
  )
}
