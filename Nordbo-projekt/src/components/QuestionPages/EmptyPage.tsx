import Parapgraph from "../Text/Parapgraph"

export default function EmptyPage({}) {
  return (
    <div className="Empty">
      <div className="CardIndexContainer">
        <div className="CardIndex"></div>
      </div>
      <div className="IndexExcluder">
        <div className="EmptyContent">
          <div className="EmptyHeaderContainer">
            <div className="EmptyHeader"></div>
          </div>
          <div className="EmptyQuestionContainer"></div>
        </div>
        <div className="ContinueButtonWrapper"></div>
      </div>
      <style jsx>
        {`
          .Empty {
            display: flex;
            padding: 230px 89px 80px 89px;
            border-bottom: 1px solid #e4e4e4;
          }
          .IndexExcluder {
            display: flex;
            flex-direction: column;
            width: 100%;
          }
          .EmptyContent {
            width: 100%;
            display: flex;
            gap: 100px;
          }
          .EmptyHeaderContainer {
            display: flex;
            width: 100%;
            margin-bottom: 32px;
          }
          .CardIndexContainer {
            margin-right: 16px;
            padding: 6px 0px;
          }
          .CardIndex {
            width: 32px;
            height: 32px;
            border-radius: 50%;
            background: #1d6afa;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 700;
          }
          .EmptyHeader {
            display: flex;
            flex-direction: column;
            gap: 8px;
          }
          .EmptyQuestionContainer {
            width: 100%;
            display: flex;
            flex-direction: column;
            gap: 16px;
          }
          .ContinueButtonWrapper {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin-top: 150px;
          }
        `}
      </style>
    </div>
  )
}
