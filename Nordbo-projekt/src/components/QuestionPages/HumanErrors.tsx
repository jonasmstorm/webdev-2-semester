import SharedLayout from "./SharedCards/SharedLayout"
import { useState, Dispatch, SetStateAction } from "react"

interface HumanErrorProps {
  next: () => void
  back: () => void
  setSelectedHumanError: Dispatch<SetStateAction<string>>
  selectedHumanError: string
  humanRef: React.RefObject<HTMLDivElement>
}

interface OptionProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
}

const options: OptionProps[] = [
  {
    text: "Yes, frequently",
    image: "thumb-up",
  },
  {
    text: "Sometimes",
    image: "thumb-pinch",
  },
  {
    text: "No, rarely",
    image: "thumb-down",
  },
]

export default function HumanError({
  next,
  back,
  setSelectedHumanError,
  selectedHumanError,
  humanRef,
}: HumanErrorProps) {
  const header =
    "Do human errors often lead to material waste due to unacceptable product tolerances?"
  const question = "Please select the answer that fits the best"
  const cardIndex = 3
  return (
    <SharedLayout
      back={back}
      next={next}
      setSelectedItem={setSelectedHumanError}
      selectedItem={selectedHumanError}
      options={options}
      header={header}
      question={question}
      cardIndex={cardIndex}
      sharedRef={humanRef}
    />
  )
}
