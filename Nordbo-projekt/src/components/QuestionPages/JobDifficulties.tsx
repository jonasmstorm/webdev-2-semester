import SharedLayout from "./SharedCards/SharedLayout"
import { useState, Dispatch, SetStateAction, RefObject } from "react"

interface JobDifficultiesProps {
  next: () => void
  back: () => void
  setSelectedJob: Dispatch<SetStateAction<string>>
  selectedJob: string
  jobRef: RefObject<HTMLDivElement>
}

interface OptionProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
}

const options: OptionProps[] = [
  {
    text: "Yes, very difficult",
    image: "thumb-up",
  },
  {
    text: "Somewhat difficult",
    image: "thumb-pinch",
  },
  {
    text: "No, not difficult",
    image: "thumb-down",
  },
]

export default function JobDifficulties({
  next,
  back,
  setSelectedJob,
  selectedJob,
  jobRef,
}: JobDifficultiesProps) {
  const header = "Is it difficult to find workers the jobs"
  const question = "Please select the answer that fits the best"
  const cardIndex = 2
  return (
    <SharedLayout
      back={back}
      next={next}
      setSelectedItem={setSelectedJob}
      selectedItem={selectedJob}
      options={options}
      header={header}
      question={question}
      cardIndex={cardIndex}
      sharedRef={jobRef}
    />
  )
}
