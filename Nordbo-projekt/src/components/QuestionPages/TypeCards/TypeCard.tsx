import Parapgraph from "../../Text/Parapgraph"
import { useState, useLayoutEffect } from "react"
import checkmark from "../../../assets/icons/checkmark.svg"
interface TypeCardProps {
  image:
    | "sanding"
    | "deburring"
    | "sprayPaint"
    | "grinding"
    | "polishing"
    | "qualityControl"
  text: string
  selected?: boolean
  onClick: () => void
}

export default function TypeCard({
  image,
  text,
  selected,
  onClick,
}: TypeCardProps) {
  const [iconSvg, setIconSvg] = useState("")
  const [loading, setLoading] = useState(false)

  //Use useLayoutEffect to dynamically import the desired icon
  useLayoutEffect(() => {
    if (!image) return
    setLoading(true)
    const importIcon = async () => {
      try {
        const importedIcon = await import(`../../../assets/icons/${image}.svg`)
        setIconSvg(importedIcon.default)
      } catch (err) {
        throw err
      } finally {
        setLoading(false)
      }
    }
    importIcon()
  }, [image])

  return (
    <div
      className={selected ? "TypeCard Active" : "TypeCard"}
      onClick={onClick}
    >
      <div
        className={
          selected ? "FloatingCheckmark Selected" : "FloatingCheckmark"
        }
      >
        {selected && (
          <img src={checkmark} alt="checkmark" className="CheckmarkIcon" />
        )}
      </div>
      <img className="TypeIcon" src={iconSvg} alt="type card" />
      <Parapgraph
        text={text}
        size="16"
        lineHeight="19"
        weight="500"
        color="#082965"
      />
      <style jsx>{`
        .TypeCard {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          position: relative;
          background: #ffffff;
          border: 1px solid #e4e4e4;
          border-radius: 8px;
          flex-basis: 32.2%;
          height: 112px;
          cursor: pointer;
        }
        .TypeCard:hover {
          border: 1px solid #1d6afa;
          background: #f6f9ff;
        }
        .FloatingCheckmark {
          display: flex;
          align-items: center;
          justify-content: center;
          position: absolute;
          width: 20px;
          height: 20px;
          right: 24px;
          top: 12px;
          background: #ffffff;
          border: 1px solid #eaeaea;
          border-radius: 50%;
        }
        .TypeIcon {
          margin-bottom: 16px;
        }
        .Selected {
          background: #1d6afa;
        }
        .Active {
          border: 2px solid #1d6afa;
        }
        .CheckmarkIcon {
          display: flex;
          align-items: center;
          justify-content: center;
          height: 12px;
          width: 12px;
        }
      `}</style>
    </div>
  )
}
