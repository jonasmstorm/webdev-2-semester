import { useState } from "react"
import ArrowRight from "../../../assets/icons/arrowRight.svg"
import Parapgraph from "../../Text/Parapgraph"
interface ButtonProps {
  icon?: boolean
  text?: string
  onClick?: () => void
  disabled?: boolean
  filled?: boolean
}

export default function CardNavigationButton({
  icon,
  text,
  onClick,
  disabled,
  filled,
}: ButtonProps) {
  return (
    <div className="CardNavigationButtonWrapper">
      <button
        className="CardNavigationButton"
        onClick={onClick}
        disabled={disabled}
        style={{
          border: "2px solid #1D6AFA",
          background: filled ? "#1d6afa" : "white",
          opacity: disabled ? "0.5" : "1",
        }}
      >
        <Parapgraph
          text={text}
          size="16"
          lineHeight="19"
          weight="600"
          color={filled ? "#FFFFFF" : "#1D6AFA"}
        />
        {icon && (
          <div className="ButtonIcon">
            <img
              src={ArrowRight}
              alt="icon arrow right"
              style={{ filter: "invert(100%)" }}
            />
          </div>
        )}
      </button>
      <style jsx>
        {`
          .CardNavigationButtonWrapper {
            display: flex;
            min-width: 49%;
            min-height: 56px;
          }
          .CardNavigationButton {
            display: flex;
            width: 100%;
            gap: 12px;
            border-radius: 8px;
            justify-content: center;
            align-items: center;
            cursor: pointer;
          }
          .ButtonIcon {
          }
        `}
      </style>
    </div>
  )
}
