import Parapgraph from "../../Text/Parapgraph"
import { useState, useLayoutEffect } from "react"
import checkmark from "../../../assets/icons/checkmark.svg"

interface SharedCardProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
  selected?: boolean
  onClick: () => void
}

export default function SharedCard({
  image,
  text,
  selected,
  onClick,
}: SharedCardProps) {
  const [iconSvg, setIconSvg] = useState("")
  const [loading, setLoading] = useState(false)

  //Use useLayoutEffect to dynamically import the desired icon
  useLayoutEffect(() => {
    if (!image) return
    setLoading(true)
    const importIcon = async () => {
      try {
        const importedIcon = await import(`../../../assets/emojis/${image}.svg`)
        setIconSvg(importedIcon.default)
      } catch (err) {
        throw err
      } finally {
        setLoading(false)
      }
    }
    importIcon()
  }, [image])

  return (
    <div
      className={selected ? "SharedCardActive" : "SharedCard"}
      onClick={onClick}
    >
      <div className="CardIconAndText">
        <img className="SharedIcon" src={iconSvg} alt="Shared card" />
        <Parapgraph
          text={text}
          size="16"
          lineHeight="19"
          weight="500"
          color="#082965"
        />
      </div>
      <div className={selected ? "CardCheckmark Selected" : "CardCheckmark"}>
        {selected && (
          <img src={checkmark} alt="checkmark" className="CheckmarkIcon" />
        )}
      </div>

      <style jsx>{`
        .SharedCard {
          display: flex;
          align-items: center;
          justify-content: space-between;
          background: #ffffff;
          border: 1px solid #e4e4e4;
          border-radius: 8px;
          cursor: pointer;
          padding: 18px;
        }
        .SharedCardActive {
          display: flex;
          align-items: center;
          justify-content: space-between;
          background: #ffffff;
          border: 2px solid #1d6afa;
          border-radius: 8px;
          flex-basis: 32.2%;
          height: 56px;
          cursor: pointer;
          padding: 18px;
        }
        .SharedCard:hover {
          border: 1px solid #1d6afa;
          background: #f6f9ff;
        }
        .CardIconAndText {
          display: flex;
          align-items: center;
          gap: 16px;
        }
        .CardCheckmark {
          display: flex;
          align-items: center;
          justify-content: center;
          width: 20px;
          height: 20px;
          right: 24px;
          top: 12px;
          background: #ffffff;
          border: 1px solid #eaeaea;
          border-radius: 50%;
          align-self: end;
        }
        .SharedIcon {
        }
        .Selected {
          background: #1d6afa;
        }
        .CheckmarkIcon {
          display: flex;
          align-items: center;
          justify-content: center;
          height: 12px;
          width: 12px;
        }
      `}</style>
    </div>
  )
}
