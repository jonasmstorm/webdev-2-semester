import { Dispatch, SetStateAction, RefObject } from "react"
import Parapgraph from "../../Text/Parapgraph"
import SharedQuestionCard from "./SharedQuestionCard"
import CardNavigationButton from "./CardNavigationButton"

interface SharedLayoutProps {
  back: () => void
  next: () => void
  setSelectedItem: Dispatch<SetStateAction<string>>
  selectedItem: string
  options: OptionProps[]
  header: string
  question: string
  cardIndex: number
  sharedRef: RefObject<HTMLDivElement>
  finalPage?: boolean
}

interface OptionProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
}

export default function SharedLayout({
  back,
  next,
  setSelectedItem,
  selectedItem,
  options,
  header,
  question,
  cardIndex,
  sharedRef,
  finalPage,
}: SharedLayoutProps) {
  return (
    <div className="Shared" ref={sharedRef}>
      <div className="CardIndexContainer">
        <div className="CardIndex">{cardIndex}</div>
      </div>
      <div className="IndexExcluder">
        <div className="SharedContent">
          <div className="SharedHeaderContainer">
            <div className="SharedHeader">
              <Parapgraph
                text={header}
                size="28"
                lineHeight="36"
                weight="700"
                color="#082965"
              />
              <Parapgraph
                text={question}
                size="16"
                lineHeight="19"
                weight="400"
                color="#818181"
              />
            </div>
          </div>
          <div className="SharedQuestionContainer">
            {options.map(({ text, image }, i) => (
              <SharedQuestionCard
                key={i}
                text={text}
                image={image}
                selected={selectedItem === text}
                onClick={() => {
                  if (selectedItem === text) {
                    setSelectedItem("")
                  } else {
                    setSelectedItem(text)
                  }
                }}
              />
            ))}
          </div>
        </div>
        <div className="ContinueButtonWrapper">
          <CardNavigationButton text="Back" onClick={() => back()} />
          <CardNavigationButton
            text={!finalPage ? "Continue" : "See results"}
            filled
            icon
            onClick={() => next()}
            disabled={!selectedItem}
          />
        </div>
      </div>
      <style jsx>
        {`
          .Shared {
            display: flex;
            padding: 230px 89px 80px 89px;
            border-bottom: 1px solid #e4e4e4;
          }
          .IndexExcluder {
            display: flex;
            flex-direction: column;
            width: 100%;
          }
          .SharedContent {
            width: 100%;
            display: flex;
            gap: 100px;
          }
          .SharedHeaderContainer {
            display: flex;
            width: 100%;
            margin-bottom: 32px;
          }
          .CardIndexContainer {
            margin-right: 16px;
            padding: 6px 0px;
          }
          .CardIndex {
            width: 32px;
            height: 32px;
            border-radius: 50%;
            background: #1d6afa;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 700;
          }
          .SharedHeader {
            display: flex;
            flex-direction: column;
            gap: 8px;
          }
          .SharedQuestionContainer {
            width: 100%;
            display: flex;
            flex-direction: column;
            gap: 16px;
          }
          .ContinueButtonWrapper {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin-top: 150px;
          }
        `}
      </style>
    </div>
  )
}
