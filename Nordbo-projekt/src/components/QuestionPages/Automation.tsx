import SharedLayout from "./SharedCards/SharedLayout"
import { useState, Dispatch, SetStateAction } from "react"

interface AutomationProps {
  next: () => void
  back: () => void
  setSelectedAutomation: Dispatch<SetStateAction<string>>
  selectedAutomation: string
  autoRef: React.RefObject<HTMLDivElement>
}

interface OptionProps {
  text: string
  image: "thumb-down" | "thumb-pinch" | "thumb-up"
}

const options: OptionProps[] = [
  {
    text: "Yes, very difficult",
    image: "thumb-up",
  },
  {
    text: "Yes, we've automated several jobs",
    image: "thumb-up",
  },
  {
    text: "Only a few jobs",
    image: "thumb-pinch",
  },
]

export default function Automation({
  next,
  back,
  setSelectedAutomation,
  selectedAutomation,
  autoRef,
}: AutomationProps) {
  const header =
    "Have the client previously tried automating some of the manual repetitive jobs?"
  const question = "Please select the answer that fits the best"
  const cardIndex = 3
  return (
    <SharedLayout
      back={back}
      next={next}
      setSelectedItem={setSelectedAutomation}
      selectedItem={selectedAutomation}
      options={options}
      header={header}
      question={question}
      cardIndex={cardIndex}
      sharedRef={autoRef}
      finalPage
    />
  )
}
