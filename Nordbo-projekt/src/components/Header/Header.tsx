import logo from "../../assets/icons/logo.svg"
import HeaderButton from "./HeaderButton"

export default function Header({
  started,
  Exit,
}: {
  started?: boolean
  Exit: () => void
}) {
  return (
    <div className="Header">
      <img className="Header-logo" src={logo} alt="logo" />
      <HeaderButton started={started} onClick={() => Exit()}></HeaderButton>
      <style jsx>
        {`
          .Header {
            display: flex;
            justify-content: space-between;
            background: white;
            padding: 24px 80px;
          }

          .Header-logo {
            height: 48px;
            width: 161px;
            object-fit: cover;
          }
        `}
      </style>
    </div>
  )
}
