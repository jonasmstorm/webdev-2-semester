import logo from "../../assets/icons/logo.svg"
import ResultHeaderButton from "./ResultHeaderButton"

export default function Header({
  started,
  RestartTool,
}: {
  started?: boolean
  RestartTool: () => void
}) {
  return (
    <div className="result-modal__header">
      <img className="result-modal__header-logo" src={logo} alt="logo" />
      <div className="result-modal__header-buttons">
        <ResultHeaderButton
          text={"Save & Share"}
          started={started}
          onClick={() => RestartTool()}
        ></ResultHeaderButton>
        <ResultHeaderButton
          text={"Restart tool"}
          started={started}
          onClick={() => RestartTool()}
        ></ResultHeaderButton>
      </div>
      <style jsx>
        {`
          .result-modal__header {
            display: flex;
            border-radius: 8px 8px 0px 0px;
            justify-content: space-between;
            background: white;
            padding: 24px 80px;
          }

          .result-modal__header-logo {
            height: 48px;
            width: 161px;
            object-fit: cover;
          }

          .result-modal__header-buttons {
            display: flex;
            gap: 30px;
          }
        `}
      </style>
    </div>
  )
}
