import Parapgraph from "../Text/Parapgraph"
interface ResultHeaderButtonProps {
  text: string
  onClick?: () => void
  disabled?: boolean
  started?: boolean
}

export default function HeaderButton({
  text,
  onClick,
  disabled,
  started,
}: ResultHeaderButtonProps) {
  return (
    <div className="result-modal__header-button-wrapper">
      <button className="result-modal__header-button" onClick={onClick} disabled={disabled}>
        <Parapgraph
          text={text}
          size="16"
          lineHeight="19"
          weight="600"
        />
      </button>
      <style jsx>
        {`
          .result-modal__header-button-wrapper {
            display: flex;
            min-width: ${started ? "auto" : "min-content"};
            transition: all 1s ease-in;
            transition: width 1s ease-in;
            transition: min-width 1s ease-in;
          }
          .result-modal__header-button {
            width: ${started ? "100%" : "auto"};
            background: white;
            border: 2px solid #1d6afa;
            padding: 0px 32px;
            height: 43px;
            border-radius: 12px;
            cursor: pointer;
            transition: width 1s ease-in;
          }
        `}
      </style>
    </div>
  )
}
