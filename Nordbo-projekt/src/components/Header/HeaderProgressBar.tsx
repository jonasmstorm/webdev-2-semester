interface HeaderProgressBarProps {
  stage: 0 | 1 | 2 | 3 | 4 | 5
}

export default function HeaderProgressBar({
  stage = 2,
}: HeaderProgressBarProps) {
  return (
    <div className="ProgressBarBackground">
      <div className="ProgressBar"></div>
      <style jsx>
        {`
          .ProgressBarBackground {
            width: 100%;
            height: 12px;
            background: #d2e1fe;
            overflow: hidden;
          }
          .ProgressBar {
            width: ${stage * 20}%;
            height: 12px;
            background: #1d6afa;
            transition: width 0.5s cubic-bezier(0, 0.5, 0.4, 1.2);
            overflow: hidden;
          }
        `}
      </style>
    </div>
  )
}
