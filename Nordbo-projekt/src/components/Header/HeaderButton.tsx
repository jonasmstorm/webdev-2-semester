import { useState, useLayoutEffect, useEffect } from "react"
import Parapgraph from "../Text/Parapgraph"
interface HeaderButtonProps {
  text?: string
  onClick?: () => void
  disabled?: boolean
  started?: boolean
}

export default function HeaderButton({
  text,
  onClick,
  disabled,
  started,
}: HeaderButtonProps) {
  return (
    <div className="ButtonWrapper">
      <button className="Button" onClick={onClick} disabled={disabled}>
        <Parapgraph
          text={started ? "Save draft & exit" : "Exit tool"}
          size="16"
          lineHeight="19"
          weight="600"
        />
      </button>
      <style jsx>
        {`
          .ButtonWrapper {
            display: flex;
            min-width: ${started ? "auto" : "min-content"};
            transition: all 1s ease-in;
            transition: width 1s ease-in;
            transition: min-width 1s ease-in;
          }
          .Button {
            width: ${started ? "100%" : "auto"};
            background: white;
            border: 2px solid #1d6afa;
            padding: 16px 32px;
            border-radius: 12px;
            cursor: pointer;
            transition: width 1s ease-in;
          }
        `}
      </style>
    </div>
  )
}
