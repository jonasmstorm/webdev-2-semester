import { useState, useLayoutEffect, useEffect } from "react"

interface ButtonProps {
  fluid?: boolean
  icon?:
    | "arrowRight"
    | "arrowDown"
    | "deburring"
    | "grinding"
    | "polish"
    | "qualityControl"
    | "sanding"
    | "sprayPaint"
  text?: string
  onClick?: () => void
  disabled?: boolean
}

export default function Button({
  fluid,
  icon,
  text,
  onClick,
  disabled,
}: ButtonProps) {
  const [iconSvg, setIconSvg] = useState("")
  const [loading, setLoading] = useState(false)

  //Use useLayoutEffect to dynamically import the desired icon
  useLayoutEffect(() => {
    if (!icon) return
    setLoading(true)
    const importIcon = async () => {
      try {
        const importedIcon = await import(`../../assets/icons/${icon}.svg`)
        setIconSvg(importedIcon.default)
      } catch (err) {
        throw err
      } finally {
        setLoading(false)
      }
    }
    importIcon()
  }, [icon])

  return (
    <div className="ButtonWrapper">
      <button onClick={onClick} disabled={disabled}>
        <div className="Button-icon">
          <img src={iconSvg} alt="icon" />
        </div>
        <div className="Button-text">{text}</div>
      </button>
      <style jsx>
        {`
          .ButtonWrapper {
            display: flex;
          }
          button {
          }
          .Button-icon {
          }
          .Button-text {
          }
        `}
      </style>
    </div>
  )
}
