# Clone repository

To clone the project please use "git clone https://gitlab.com/jonasmstorm/webdev-2-semester.git"

## Installation

After you clone the repository cd into Nordbo-projekt and run following

```bash
npm install
```

## dev

After installing packages run the following command to start project

```bash
npm run dev
```